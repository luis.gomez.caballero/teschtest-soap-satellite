package techtest.soapsat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapSatelliteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapSatelliteApplication.class, args);
	}

}
