package techtest.soapsat.endpoint;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import techtest.soap.helloservice.GetHelloRequest;
import techtest.soap.helloservice.GetHelloResponse;

@Endpoint
public class HelloEndpoint {
	private static final String NAMESPACE_URI = "techtest/soap/helloservice";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getHelloRequest")
	@ResponsePayload
	public GetHelloResponse getHello(@RequestPayload GetHelloRequest request) {

		GetHelloResponse response = new GetHelloResponse();
		String name = request.getName();

		if ("".equals(name)) {
			name = "World";
		}

		response.setMessage("Hello " + name + " from the SOAP Server!");

		return response;

	}
}
